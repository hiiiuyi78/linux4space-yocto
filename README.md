# Linux4Space Yocto
#Linux4Space - The yocto layers 

This is the main repository for the yocto-based Linux distribution created within the Linux4Space project.
Detailed information can be found at www.linux4space.org.

The project is starting in February 2023, the first output shall be ready in the June 2023.